/** 
 * Copyright (c) 2012,　六动力（福建）网络科技有限公司  All rights reserved。
 * 
 * ITestDao.java
 */

package test.dao;

/**  
 *
 * @author zhys
 * @date 2014-5-8 上午11:24:41 
 */
public interface ITestDao {
    String test();
}
