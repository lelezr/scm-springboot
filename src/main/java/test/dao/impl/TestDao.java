/** 
 * Copyright (c) 2012,　六动力（福建）网络科技有限公司  All rights reserved。
 * 
 * TestDao.java
 */

package test.dao.impl;

import org.springframework.stereotype.Repository;

import test.dao.ITestDao;

/**  
 *
 * @author zhys
 * @date 2014-5-8 上午11:23:55 
 */
@Repository
public class TestDao implements ITestDao {

    @Override
    public String test() { 
        java.util.Random r=new java.util.Random(); 
        String s = String.valueOf(r.nextInt());
        return s;
    }
     
}
